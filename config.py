# Application Directory
import logging
import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Host and Port
HOST = '0.0.0.0'
PORT = 8080
DEBUG = True

MONGO_HOST = '172.16.217.98'
MONGO_PORT = 27017

REDIS_HOST = "172.16.217.98"
REDIS_PORT = 6379

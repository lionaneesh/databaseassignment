from flask import render_template, request
from app import app
from helper_functions import get_documents

@app.route('/', methods=['GET', 'POST'])
def show_page():
    if request.method == 'GET':
        return render_template('query.html')
    else:
        query_results = get_documents(request.form)
        return render_template('query_response.html',results=query_results)
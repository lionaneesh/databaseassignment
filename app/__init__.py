from flask import Flask

#Define WSGI object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Views
import views
import pymongo
from app import app
import json
import redis
import cPickle

def get_db():
    """ Connect to MongoDB and select the database"""
    client = pymongo.MongoClient(app.config['MONGO_HOST'], app.config['MONGO_PORT'])
    db = client.airline
    return db


def get_query_params(form_args):
    """ Filter out the form arguments and keep only valid ones.
        Forms the query string accordingly.
    """
    query_params = {}
    for arg, value in form_args.items():
        if value and not arg.endswith('Opr'):
            arg_comp_val = form_args.get(arg + '-Opr')

            if arg_comp_val == "greater than":
                param_val = {"$gt": int(value)}
            elif arg_comp_val == "lesser than":
                param_val = {"$lt": int(value)}
            elif  arg_comp_val == "equal to":
                param_val = int(value)
            else:
                param_val = value

            query_params[arg] = param_val
    return query_params


def get_documents_from_db(form_args):
    """ Select the db and fetch documents from MongoDB"""
    db = get_db()
    query_args = get_query_params(form_args)
    result_cursor = db.air_data.find(query_args).limit(50).sort("Year", pymongo.DESCENDING)

    results = []
    for record in result_cursor:
        results.append(record)

    return results


def get_documents(form_args):
    """ Checks if the query is there in Redis. If it is, server from there. Else make the query to MongoDB"""
    redis_conn= redis.StrictRedis(host=app.config['REDIS_HOST'], port=app.config['REDIS_PORT'], db=0)
    hash_key = hash(json.dumps(form_args,sort_keys=True))

    # Check if data is in cache.
    if (redis_conn.get(hash_key)):
        print "This was return from redis"
        return cPickle.loads(redis_conn.get(hash_key))
    else:
        results = get_documents_from_db(form_args)
        redis_conn.set(hash_key, cPickle.dumps(results))

        return results
